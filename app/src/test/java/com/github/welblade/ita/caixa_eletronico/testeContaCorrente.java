package com.github.welblade.ita.caixa_eletronico;

import static org.junit.jupiter.api.Assertions.*;

import com.github.welblade.ita.caixa_eletronico.helper.MockServicoRemoto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class testeContaCorrente {
    MockServicoRemoto servico;
    ContaCorrente conta;

    @BeforeEach
    public void inicializar(){
        servico = new MockServicoRemoto();
        conta = new ContaCorrente(servico);
    }

    @Test
    public void testarCriacaoObjetoContaCorrente(){
        assertNotNull(conta.getServico());
    }

    @Test
    public void testarLoginContaCorrente(){
        assertTrue(conta.logar("123456-78"));
        assertEquals("123456-78", conta.getNumero());
        assertEquals(1000.0, conta.getSaldo());
    }

    @Test
    public void criarContaComServicoNulo(){
        servico = null;
        assertThrows(
            NullPointerException.class, 
            () ->  new ContaCorrente(servico)
        );
    }

    @Test
    public void loginContaInexistente(){
        assertFalse(conta.logar("999999-99"));
    }

    @Test
    public void fazerDeposito(){
        conta.logar("123456-78");
        assertTrue(conta.depositar(200.0));
        assertEquals(1, servico.chamadasPersistirConta());
        assertEquals(1200.0, conta.getSaldo());
    }

    @Test
    public void fazerDepositoSemLogar(){
        assertThrows(OperacaoSemLogarException.class, () -> conta.depositar(200.0)); 
    }

    @Test
    public void fazerDepositoNegativo(){
        conta.logar("123456-78");
        assertThrows(OperacaoValorNegativoException.class, () -> conta.depositar(-200.0));
    }

    @Test
    public void fazerUmSaque(){
        conta.logar("123456-78");
        assertTrue(conta.sacar(200.0));
        assertEquals(1, servico.chamadasPersistirConta());
        assertEquals(800.0, conta.getSaldo());
    }

    @Test
    public void fazerSaqueSemLogar(){
        conta.logar("999999-99");
        assertThrows(OperacaoSemLogarException.class, () -> conta.sacar(1000));
    }

    @Test
    public void fazerSaqueAlemDoLimite(){
        conta.logar("123456-78");
        assertThrows(OperacaoInvalidaException.class, () -> conta.sacar(1000.1));
    }

    @Test
    public void fazerSaqueNegativo(){
        conta.logar("123456-78");
        assertThrows(OperacaoValorNegativoException.class, () -> conta.sacar(-100));
    }
}