package com.github.welblade.ita.pilha;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import com.github.welblade.ita.pilha.exceptions.PilhaCheiaException;
import com.github.welblade.ita.pilha.exceptions.PilhaVaziaException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestePilha {
    private Pilha p;

    @BeforeEach
    public void inicializaPilha(){
        this.p = new Pilha(10);
    }

    @Test
    public void pilhaVazia(){
        assertTrue(p.estaVazia());
        assertEquals(0, p.tamanho());
    }
    @Test
    public void empilharUmElemento(){
        Pilha p = new Pilha(10);
        p.empilha("primeiro");
        assertFalse(p.estaVazia());
        assertEquals(1, p.tamanho());
        assertEquals("primeiro", p.topo());
    }

    @Test
    public void empilharDoisElementosDesemplihaUm(){
        Pilha p = new Pilha(10);
        p.empilha("primeiro");
        p.empilha("segundo");
        assertFalse(p.estaVazia());
        assertEquals(2, p.tamanho());
        assertEquals("segundo", p.topo());
        Object desempilhado = p.desempihar();
        assertFalse(p.estaVazia());
        assertEquals(1, p.tamanho());
        assertEquals("primeiro", p.topo());
        assertEquals("segundo", desempilhado);
    }

    @Test
    public void removeDaPilhaVazia(){
        assertThrows(PilhaVaziaException.class, () -> p.desempihar()) ;
    }

    @Test
    public void empilhaEmPilhaCheia(){
        try{
            for(var i = 1; i <= 10; i++){
                p.empilha(i);
            }
        }catch(Exception e){
            fail("A Excessão aconteceu antes do esperado." + e.getLocalizedMessage());
        }

        assertThrows(PilhaCheiaException.class, () -> p.empilha(11)) ;
    }

}
