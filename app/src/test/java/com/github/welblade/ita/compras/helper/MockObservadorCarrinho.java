package com.github.welblade.ita.compras.helper;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.github.welblade.ita.compras.ObservadorCarrinho;

public class MockObservadorCarrinho implements ObservadorCarrinho {
    private String nome;
    private int valor;
    private boolean ehPraDarErro = false;
    public void produtoAdicionado(String nome, int valor){
        if(ehPraDarErro)
            throw new RuntimeException("Missão cumprida, um erro forçado!");
        this.nome = nome;
        this.valor = valor;
    }

    public void verificarRecebimentoProduto(String nomeEsperado, int valorEsperado){
        assertEquals(nomeEsperado, this.nome);
        assertEquals(valorEsperado, this.valor);
    }

    public void forcarErro() {
        this.ehPraDarErro = true;
    }
}
