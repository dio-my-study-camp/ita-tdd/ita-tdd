package com.github.welblade.ita.caixa_eletronico;

import static org.junit.jupiter.api.Assertions.*;

import com.github.welblade.ita.caixa_eletronico.helper.HardwareMock;
import com.github.welblade.ita.caixa_eletronico.helper.HardwareMockContaInexistente;
import com.github.welblade.ita.caixa_eletronico.helper.HardwareMockDefeituoso;
import com.github.welblade.ita.caixa_eletronico.helper.MockServicoRemoto;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class TesteCaixaEletronico {
    MockServicoRemoto servico;
    ContaCorrente conta;
    HardwareMock hardware;

    @BeforeEach
    public void inicilizar(){
        servico = new MockServicoRemoto();
        conta = new ContaCorrente(servico);
        hardware = new HardwareMock();
    }

    @Test
    public void criacaoDeObjeto(){
        var caixa = new CaixaEletronico(conta, hardware);
        assertNotNull(caixa.getConta());
        assertNotNull(caixa.getHardware());
    }

    @Test
    public void criacaoDeObjetoComContaNula(){
        conta = null;
        assertThrows(NullPointerException.class, () -> new CaixaEletronico(conta, hardware));
    }

    @Test
    public void criacaoDeObjetoComHardwareNulo(){
        hardware = null;
        assertThrows(NullPointerException.class, () -> new CaixaEletronico(conta, hardware));
    }

    @Test
    public void executarLogin(){
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        assertEquals("Usuário Autenticado", caixa.logar());
        assertEquals(1L, hardware.contaChamadasMetodo("pegarNumeroDaContaCartao"));
        assertEquals(1000.0, caixa.getConta().getSaldo());
    }

    @Test
    public void executarLoginContaInexistente(){
        hardware = new HardwareMockContaInexistente();
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        assertEquals("Não foi possível autenticar o usuário", caixa.logar());
        assertEquals(1L, hardware.contaChamadasMetodo("pegarNumeroDaContaCartao"));
    }

    @Test
    public void executarLoginFalhaAoLerCartao(){
        hardware = new HardwareMockDefeituoso();
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        assertEquals("Não foi possível autenticar o usuário", caixa.logar());
    }

    @Test
    public void recuperarSaldo(){
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        caixa.logar();
        assertEquals("O saldo é R$ 1.000,00", caixa.saldo());
    }

    @Test
    public void executarSaque(){
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        caixa.logar();
        assertEquals("Retire seu dinheiro", caixa.sacar(200.0));
        assertEquals(1L, hardware.contaChamadasMetodo("entregarDinheiro"));
        assertEquals("O saldo é R$ 800,00", caixa.saldo());
    }

    @Test
    public void executarSaqueSaldoInsuficiente(){
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        caixa.logar();
        assertEquals("Saldo insuficiente", caixa.sacar(1200.0));
        assertEquals("O saldo é R$ 1.000,00", caixa.saldo());
    }

    @Test
    public void executarSaqueErroEntregaDinheiro(){
        HardwareMockDefeituoso hardwareDefeituoso = new HardwareMockDefeituoso();
        hardwareDefeituoso.permitaLerCartao();
        CaixaEletronico caixa = new CaixaEletronico(conta, hardwareDefeituoso);
        caixa.logar();
        assertEquals("Erro no dispenser de notas.", caixa.sacar(200.0));
        assertEquals("O saldo é R$ 1.000,00", caixa.saldo());
    }

    @Test
    public void executarDeposito(){
        CaixaEletronico caixa = new CaixaEletronico(conta, hardware);
        caixa.logar();
        assertAll(
            () -> assertEquals("Depósito recebido com sucesso", caixa.depositar(200.0)),
            () -> assertEquals(1L, hardware.contaChamadasMetodo("lerEnvelope")),
            () -> assertEquals("O saldo é R$ 1.200,00", caixa.saldo())
        );
    }

    @Test
    public void executarDepositoErroLendoEnvelope(){
        HardwareMockDefeituoso hardwareDefeituoso = new HardwareMockDefeituoso();
        hardwareDefeituoso.permitaLerCartao();
        CaixaEletronico caixa = new CaixaEletronico(conta, hardwareDefeituoso);
        caixa.logar();
        assertAll(
            () -> assertEquals("Erro no coletor de envelope.", caixa.depositar(200.0)),
            () -> assertEquals("O saldo é R$ 1.000,00", caixa.saldo())
        );
    }


}
