package com.github.welblade.ita.caixa_eletronico.helper;

import java.util.ArrayList;
import java.util.List;

import com.github.welblade.ita.caixa_eletronico.Hardware;

public class HardwareMock implements Hardware {
    protected List<String> chamadasMetodos = new ArrayList<>();
    @Override
    public String pegarNumeroDaContaCartao() {
        chamadasMetodos.add("pegarNumeroDaContaCartao");
        return "123456-78";
    }

    @Override
    public void entregarDinheiro() {
        chamadasMetodos.add("entregarDinheiro");    
    }

    @Override
    public void lerEnvelope() {
        chamadasMetodos.add("lerEnvelope");
    }

    public long contaChamadasMetodo(String metodo){
        return chamadasMetodos.stream().filter(elemento -> elemento.equals(metodo)).count();
    }

}
