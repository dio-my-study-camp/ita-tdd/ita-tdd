package com.github.welblade.ita.tradutor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class tradutorTeste {
    Tradutor t;

    @BeforeEach
    public void criarTradutor(){
        this.t = new Tradutor();
    }

    @Test
    @DisplayName("1. Tradutor sem palavra.")
    public void tradutorSemPalavra() {
        assertTrue(t.estaVazio());
    }

    @Test
    @DisplayName("2. Adicionar e traduzir uma palavra.")
    public void umaTraducao(){
        t.adicionaTraducao("bom", "good");

        assertFalse(t.estaVazio());
        assertEquals("good", t.traduzir("bom"));
    }

    @Test
    @DisplayName("3. Adicionar e traduzir duas palavras.")
    public void duasTraducoes(){
        t.adicionaTraducao("bom", "good");
        t.adicionaTraducao("mau", "bad");

        assertEquals("good", t.traduzir("bom"));
        assertEquals("bad", t.traduzir("mau"));
    }

    @Test
    @DisplayName("4. Adicionar duas traduções para uma palavra e traduzir.")
    public void duasTraducoesMesmaPalavra(){
        t.adicionaTraducao("bom", "good");
        t.adicionaTraducao("bom", "nice");

        assertEquals("good, nice", t.traduzir("bom"));
    }

    @Test
    @DisplayName("5. Traduzir uma frase.")
    public void traduzirFrase(){
        t.adicionaTraducao("guerra", "war");
        t.adicionaTraducao("é", "is");
        t.adicionaTraducao("ruim", "bad");
        assertEquals("war is bad", t.traduzirFrase("guerra é ruim"));
    }

    @Test
    @DisplayName("5. Traduzir uma frase com uma palavra que possue duas traduções.")
    public void traduzirFraseComPalavraComDuasTraducoes(){
        t.adicionaTraducao("paz", "peace");
        t.adicionaTraducao("é", "is");
        t.adicionaTraducao("bom", "good");
        t.adicionaTraducao("bom", "nice");
        assertEquals("peace is good", t.traduzirFrase("paz é bom"));
    }
}
