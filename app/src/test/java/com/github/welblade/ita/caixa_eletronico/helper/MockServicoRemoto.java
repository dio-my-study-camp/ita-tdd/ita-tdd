package com.github.welblade.ita.caixa_eletronico.helper;

import java.util.HashMap;
import java.util.Map;

import com.github.welblade.ita.caixa_eletronico.Conta;
import com.github.welblade.ita.caixa_eletronico.ServicoRemoto;

public class MockServicoRemoto implements ServicoRemoto {
    private int quantidadeChamadasPersistir = 0;
    @Override
    public Map<String, Object> recuperarConta(String numeroConta) {
        if(numeroConta == "123456-78"){
            Map<String, Object> conta = new HashMap<String,Object>();
            conta.put("numero", "123456-78");
            conta.put("saldo", 1000.0);
            return conta;
        }
        return null;
    }

    @Override
    public boolean persistirConta(Conta conta) {
        quantidadeChamadasPersistir++;
        return true;
    }

    public int chamadasPersistirConta() {
        return quantidadeChamadasPersistir;
    }

}
