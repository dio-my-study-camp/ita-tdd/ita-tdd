package com.github.welblade.ita.caixa_eletronico.helper;

public class HardwareMockDefeituoso extends HardwareMock {
    private boolean permiteLerCartao = false;
    @Override
    public String pegarNumeroDaContaCartao(){
        if(permiteLerCartao)
            return super.pegarNumeroDaContaCartao();
        throw new RuntimeException("Erro forçado ao ler cartão.");
    }
    public String pegarNumeroDaContaCartaoSemErro(){
        return super.pegarNumeroDaContaCartao();
    }
    @Override
    public void entregarDinheiro() {
        throw new RuntimeException(); 
    }

    @Override
    public void lerEnvelope() {
        throw new RuntimeException();
    }

    public void permitaLerCartao(){
        permiteLerCartao = true;
    }
}
