package com.github.welblade.ita.camelcase;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;

@TestInstance(Lifecycle.PER_CLASS)
public class CamelCaseTest {
    @Test
    public void deveRetornarUmaPalavra(){
        assertEquals(1, CamelCase.split("nome").size());
    }
    @Test
    public void deveRetornarUmaPalavraMinuscula(){
        List<String> expected = new ArrayList<String>();
        expected.add("nome");
        assertArrayEquals(expected.toArray(), CamelCase.split("Nome").toArray());
    }
    @Test
    public void deveRetornarDuasPalavras(){
        List<String> expected = new ArrayList<String>();
        expected.add("nome");
        expected.add("composto");
        assertEquals(2, CamelCase.split("nomeComposto").size());
    }

    @Test
    public void deveRetornarDuasPalavrasMinusculas(){
        List<String> expected = new ArrayList<String>();
        expected.add("nome");
        expected.add("composto");
        assertArrayEquals(expected.toArray(), CamelCase.split("NomeComposto").toArray());
    }
    @Test
    public void deveRetornarUmaPalavraMaiuscula(){
        List<String> expected = new ArrayList<String>();
        expected.add("CPF");
        assertArrayEquals(expected.toArray(), CamelCase.split("CPF").toArray());
    }

    //numeroCPF - “numero”, “CPF” 
    @Test
    public void deveRetornarUmaMinusculaUmaMaiuscula(){
        List<String> expected = new ArrayList<String>();
        expected.add("numero");
        expected.add("CPF");
        assertArrayEquals(expected.toArray(), CamelCase.split("numeroCPF").toArray());
    }
    @Test
    public void deveRetornarDuasMinusculasUmaMaiuscula(){
        List<String> expected = new ArrayList<String>();
        expected.add("numero");
        expected.add("CPF");
        expected.add("contribuinte");
        assertArrayEquals(expected.toArray(), CamelCase.split("numeroCPFContribuinte").toArray());
    }

    @Test
    public void deveRetornarDuasMinusculasUmNumero(){
        List<String> expected = new ArrayList<String>();
        expected.add("recupera");
        expected.add("10");
        expected.add("primeiros");
        assertArrayEquals(expected.toArray(), CamelCase.split("recupera10Primeiros").toArray());
    }
    // 10Primeiros - Inválido → não deve começar com números 
    @Test
    public void deveFalharPorIniciarComNumero(){
        assertThrows(InvalidStringFormatException.class, () -> CamelCase.split("10Primeiros"));
    }
    // nome#Composto - Inválido → caracteres especiais não são permitidos, somente letras e números
    @Test
    public void deveFalharPorConterCaracterEspecial(){
        assertThrows(InvalidStringFormatException.class, () -> CamelCase.split("nome#Composto"));
    }
}
