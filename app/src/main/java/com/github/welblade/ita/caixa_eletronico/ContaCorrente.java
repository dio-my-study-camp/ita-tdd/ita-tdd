package com.github.welblade.ita.caixa_eletronico;

import java.util.Map;

public class ContaCorrente implements Conta{
    private ServicoRemoto servicoRemoto;
    private String numero;
    private double saldo;

    public ContaCorrente(ServicoRemoto servicoRemoto) {
        if(servicoRemoto == null)
            throw new NullPointerException(
                "Não é possível utilizar um ServiçoRemoto nulo " +
                "para inicializar a conta corrente.");
        this.servicoRemoto = servicoRemoto;
	}

	@Override
    public boolean logar(String numeroConta){
        Map<String, Object> informacoes = 
            servicoRemoto.recuperarConta(numeroConta);
        
        if(informacoes == null) return false;

        this.numero = (String) informacoes.get("numero");
        this.saldo = (double) informacoes.get("saldo");
        return true;
    }

    @Override
    public boolean sacar(double valor) {
        if(numero == null)
            throw new OperacaoSemLogarException(
                "Não é possível fazer saques sem antes logar a uma conta."
            );

        if(valor < 0.0)
            throw new OperacaoValorNegativoException(
                "Não é possível fazer saques de valores negativos."
            );
        if(valor > getSaldo())
            throw new OperacaoInvalidaException(
                "Não é posível Fazer um saque que excede o saldo disponível."
            );
        this.saldo -= valor;
        servicoRemoto.persistirConta(this);
        return true;
    }

    @Override
    public boolean depositar(double valor) {
        if(numero == null)
            throw new OperacaoSemLogarException(
                "Não é possível fazer depósitos sem antes logar a uma conta."
            );
        if(valor < 0.0)
            throw new OperacaoValorNegativoException(
                "Não é possível fazer depósitos de valores negativos."
            );
        saldo += valor;
        return servicoRemoto.persistirConta(this);
    }

    @Override
    public Object getServico() {
        return servicoRemoto;
    }

    @Override
    public Object getNumero() {
        return numero;
    }

    @Override
    public Double getSaldo() {
        return saldo;
    }

}
