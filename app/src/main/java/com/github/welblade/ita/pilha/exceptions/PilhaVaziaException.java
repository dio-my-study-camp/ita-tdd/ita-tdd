package com.github.welblade.ita.pilha.exceptions;


public class PilhaVaziaException extends RuntimeException {
    public PilhaVaziaException(String msg){
        super(msg);
    }
}
