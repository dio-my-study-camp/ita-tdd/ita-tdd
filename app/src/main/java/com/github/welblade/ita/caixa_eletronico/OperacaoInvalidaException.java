package com.github.welblade.ita.caixa_eletronico;

public class OperacaoInvalidaException extends RuntimeException{
    public OperacaoInvalidaException(String mensagem){
        super(mensagem);
    }
}
