package com.github.welblade.ita.compras;

import java.util.ArrayList;
import java.util.List;

public class CarrinhoCompras {
        private List<Produto> itens = new ArrayList<>();
        private List<ObservadorCarrinho> observadores = new ArrayList<>();
        public void adicionarProduto(Produto p){
            itens.add(p);
            notificarObservadores(p);
        }

        private void notificarObservadores(Produto p) {
            for(var observador : observadores){
                try {
                    observador.produtoAdicionado(
                        p.getNome(), 
                        p.getValor()
                    );
                } catch (RuntimeException err) {}
            }
        }

        public int total(){
            int total = 0;
            for(Produto p : itens){
                total += p.getValor();
            }
            return total;
        }

        public void adicionarObservador(ObservadorCarrinho observador) {
            this.observadores.add(observador);
        }
}
