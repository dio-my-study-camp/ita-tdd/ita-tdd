package com.github.welblade.ita.caixa_eletronico;

import java.util.Map;

public interface ServicoRemoto {
    public Map<String, Object> recuperarConta(String numeroConta);
    public boolean persistirConta(Conta conta);
}
