package com.github.welblade.ita.caixa_eletronico;

import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.Normalizer.Form;

public class CaixaEletronico {
    private Conta conta;
    private Hardware hardware;
    public CaixaEletronico(Conta conta, Hardware hardware) {
        if(conta == null)
            throw new NullPointerException("Objeto Conta não pode ser nulo.");
        if(hardware == null)
            throw new NullPointerException("Objeto Hardware não pode ser nulo.");
        this.conta = conta;
        this.hardware = hardware;
    }

    public Conta getConta() {
        return conta;
    }

    public Hardware getHardware() {
        return hardware;
    }

    public String logar() { 
        try {
            var numeroConta = hardware.pegarNumeroDaContaCartao();
            if(conta.logar(numeroConta)){
                return "Usuário Autenticado";
            }
        } catch (RuntimeException e) {
            System.out.println("Error" + e.getMessage());
        }
        return "Não foi possível autenticar o usuário";
    }

    public String saldo() {
        return Normalizer.normalize(
            "O saldo é " + 
            NumberFormat.getCurrencyInstance().format(
                conta.getSaldo()
            ), 
            Form.NFKC
        );
    }

    public String sacar(double valor) {
        try {
            hardware.entregarDinheiro();
            conta.sacar(valor);
        } catch (OperacaoInvalidaException err) {
            return "Saldo insuficiente";
        } catch (RuntimeException err){
            return "Erro no dispenser de notas.";
        }
        
        return "Retire seu dinheiro";
    }

    public String depositar(double valor) {
        try {
            hardware.lerEnvelope();
            conta.depositar(valor);
            return "Depósito recebido com sucesso";
        } catch (RuntimeException err) {
            return "Erro no coletor de envelope.";
        }
    }

}
