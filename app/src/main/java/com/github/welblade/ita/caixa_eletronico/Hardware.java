package com.github.welblade.ita.caixa_eletronico;

public interface Hardware {
    public String pegarNumeroDaContaCartao();
    public void entregarDinheiro();
    public void lerEnvelope();
}
