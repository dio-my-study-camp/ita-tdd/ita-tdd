package com.github.welblade.ita.caixa_eletronico;

public class OperacaoSemLogarException extends RuntimeException{
    public OperacaoSemLogarException(String mensagem){
        super(mensagem);
    }
}
