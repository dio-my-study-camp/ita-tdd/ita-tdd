package com.github.welblade.ita.camelcase;
import java.util.ArrayList;
import java.util.List;

public class CamelCase {
    public static List<String> split(final String string) {
        validateStringFormat(string);
        List<String> words = splitAtUpperCase(string);       
        return convertAllElementsToLowerCase(words);
    }

    private static List<String> splitAtUpperCase(final String string){
        List<String> words = new ArrayList<String>();
        StringBuilder word = new StringBuilder();

        for(var index = 0; index < string.length(); index++){
            Character letter = string.charAt(index);

            if((isUpperCase(letter) || isNumber(letter)) 
                && word.length() > 0
            ){
                if((!isUpperCase(word.toString())
                     && !isNumber(word.toString())
                )
                || (index + 1 < string.length() 
                    && !isUpperCase(string.charAt(index + 1))
                    && !isNumber(string.charAt(index + 1))
                )){
                    words.add(word.toString());
                    word = new StringBuilder();
                }
            }
            word.append(letter);
        }
        words.add(word.toString());
        return words;
    }

    private static List<String> convertAllElementsToLowerCase(List<String> list){
        List<String> words = new ArrayList<String>();
        list.forEach((word) -> { 
            if(isUpperCase(word)){
                words.add(word);
            } else {
                words.add(word.toLowerCase());
            }
        });
        return words;
    }

    private static boolean isUpperCase(Character character){
        return isUpperCase(character.toString());
    }

    private static boolean isUpperCase(String string){
        return string.matches("^[A-Z]*$");
    }

    private static boolean isNumber(Character character){
        return isNumber(character.toString());
    }

    private static boolean isNumber(String string){
        return string.matches("^[0-9]*$");
    }

    private static void validateStringFormat(String string){
        if(isStartingWithNumber(string)){
            throw new InvalidStringFormatException("The string can't start with numbers.");
        }
        if(!containsOnlyAlphaAndNumbers(string)){
            throw new InvalidStringFormatException("Invalid caracter on the given string.");
        }
    }

    private static boolean isStartingWithNumber(String string){
        return isNumber(string.charAt(0));
    }

    private static boolean containsOnlyAlphaAndNumbers(String string){
        return string.matches("^[a-zA-Z0-9]*$");
    }
}
