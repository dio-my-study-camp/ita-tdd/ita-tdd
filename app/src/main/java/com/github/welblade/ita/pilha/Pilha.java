package com.github.welblade.ita.pilha;

import com.github.welblade.ita.pilha.exceptions.PilhaCheiaException;
import com.github.welblade.ita.pilha.exceptions.PilhaVaziaException;

public class Pilha {
    private Object[] elementos = new Object[10];
    private int quantidade = 0;

    public Pilha(int i) {
    }

    public boolean estaVazia() {
        return (this.quantidade == 0);
    }

    public int tamanho() {
        return this.quantidade;
    }

    public Object topo() {
        return this.elementos[this.quantidade-1];
    }

    public void empilha(Object elemento) {
        if(this.quantidade +1 > elementos.length ){
            throw new PilhaCheiaException(
                "Não é mais possível adicionar nenhum elemento, pilha cheia."
            );
        }
        this.elementos[this.quantidade] = elemento;
        this.quantidade++;
    }

    public Object desempihar() {
        if(estaVazia()){
            throw new PilhaVaziaException(
                "Não existe mais elementos para desempilhar"
            );
        }

        this.quantidade--;
        Object elemento = this.elementos[this.quantidade];
        this.elementos[this.quantidade] = null;
        return elemento;
    }

}
